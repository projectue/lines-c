﻿
#include <iostream>
#include <string>

int main()
{
    //инициализация строки переменной
    std::string variable = "Hello new world";

    //вывод переменной на экран
    std::cout << variable << "\n"; 

    // вывод длины переменной
    std::cout << variable.length() << "\n";
    
    //Вывод первой буквы строки
   std::cout << variable.front() << "\n";

    //Вывод последней буквы строки
   std::cout << variable.back()<< "\n";
}